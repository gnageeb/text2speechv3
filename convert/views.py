from django.shortcuts import render
from .models import Language, AvailableLanguages
import os
import re

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "static/keys/TexttoSpeach-23a2eacb5d47.json"
"""Lists the available voices."""
from google.cloud import texttospeech
# from google.cloud.texttospeech import enums

client = texttospeech.TextToSpeechClient()
import datetime


def index(request):
    av_langs = AvailableLanguages.objects.all()
    langs = []
    for item in av_langs:
        try:
            res = Language.objects.get(language_code=item.language_code)
            langs.append(res)
        except:
            pass
    return render(request, 'index.html', {'langs': langs})


# all_voices = []

def load_types(request):
    # Performs the list voices request
    langpk = request.GET.get('langpk')
    language_code = Language.objects.get(pk=langpk).language_code
    voices = client.list_voices()
    types = []
    for voice in voices.voices:
        details = voice.name.split('-')
        # all_voices.append({'language_code':details[0]+'-'+details[1],
        #                    'type': details[2],
        #                    'name': details[3],
        #                    'gender': 'FEMALE' if voice.ssml_gender == 2 or voice.ssml_gender == 'FEMALE' else 'MALE'})
        type = details[2]
        if type not in types:
            types.append(type)
    return render(request, 'sub/types_dll.html', {'types': types})


def load_genders(request):
    # Performs the list voices request
    langpk = request.GET.get('langpk')
    selected_type = request.GET.get('type')
    language_code = Language.objects.get(pk=langpk).language_code
    voices = client.list_voices()
    genders = []
    for voice in voices.voices:
        details = voice.name.split('-')
        type = details[2]
        if voice.ssml_gender == 2:
            gender = 'FEMALE'
        elif voice.ssml_gender == 1:
            gender = 'MALE'
        else:
            gender = 'NEUTRAL'

        # check only voices with same voice type
        if type == selected_type and gender not in genders:
            genders.append(gender)

    return render(request, 'sub/genders_radio.html', {'genders': genders})


def load_names(request):
    # Performs the list voices request
    langpk = request.GET.get('langpk')
    selected_type = request.GET.get('type')
    selected_gender = request.GET.get('gender')
    language_code = Language.objects.get(pk=langpk).language_code
    voices = client.list_voices()
    names = []
    for voice in voices.voices:
        details = voice.name.split('-')
        type = details[2]
        if voice.ssml_gender == 2:
            gender = 'FEMALE'
        elif voice.ssml_gender == 1:
            gender = 'MALE'
        else:
            gender = 'NEUTRAL'
        name = voice.name
        # check only voices with same voice type
        if type == selected_type and gender == selected_gender and name not in names and language_code in voice.name:
            names.append(name)

    return render(request, 'sub/names_dll.html', {'names': names})


def convert(request):
    langpk = request.GET.get('langpk')
    text = request.GET.get('text')
    name = request.GET.get('name')
    gender = request.GET.get('gender')

    ssml_gender = texttospeech.SsmlVoiceGender.FEMALE if gender == "FEMALE" else texttospeech.SsmlVoiceGender.MALE

    language_code = Language.objects.get(pk=langpk).language_code
    # Set the text input to be synthesized
    ssml = re.sub(r"(\d[.:)\-])", r'\1<break time="400ms"/>', text)  # replace numbering with stop
    ssml = re.sub(r"(\D[,;:]+)", r'\1<break time="400ms"/>', ssml)
    ssml = "<speak>" + ssml + "</speak>"
    # Instantiates a client
    # client = texttospeech.TextToSpeechClient()

    # Set the text input to be synthesized
    synthesis_input = texttospeech.SynthesisInput(text=text)

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.VoiceSelectionParams(
        language_code=language_code, ssml_gender=ssml_gender
    )

    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )

    now = datetime.datetime.now()
    output_fname = now.strftime("%Y-%m-%d-%H%M%S") + '.mp3'
    # The response's audio_content is binary.
    with open('static/output/' + output_fname , 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file ' + output_fname)
    return render(request, 'sub/audio_player.html', {'file': 'output/' + output_fname})
