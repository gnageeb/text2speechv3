from bs4 import BeautifulSoup
import requests
import json
import pandas as pd

website_url = requests.get("https://cloud.google.com/speech-to-text/docs/languages").text
soup = BeautifulSoup(website_url, "lxml")
my_table = soup.find("table")
data = []
all_rows = my_table.findAll("tr")
pk = 0
for row in all_rows[1:]:
    pk += 1
    cells = row.findAll("td")
    data.append({
        "model": "convert.Language",
        "pk": pk,
        "fields": {
            "language_name": cells[0].text,
            "language_code": cells[1].text,
            "language_en_name": cells[2].text
        }
    })
with open('data.json', 'w') as outfile:
    json.dump(data, outfile)

# api-endpoint

URL = "https://texttospeech.googleapis.com/v1/voices?key=AIzaSyCBgrSrddfXZApYUdvfOFxqS4mu-GGzmI0"

# sending get request and saving the response as response object
r = requests.get(url=URL)

# extracting data in json format
data = r.json()
df = pd.DataFrame(data["voices"])
languages = df.languageCodes.value_counts().index  # to get all languages
availableLangs = pd.DataFrame()
availableLangs["languageCode"] = languages.str[0]
availableLangs["model"] = "convert.AvailableLanguages"
availableLangs["pk"] = availableLangs.index
data = availableLangs.to_json(orient='records')
with open('data2.json', 'w') as outfile:
    json.dump(data, outfile)
