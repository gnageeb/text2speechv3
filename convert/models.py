from django.db import models


class Language(models.Model):
    language_name = models.CharField(max_length=120)
    language_code = models.CharField(max_length=120)
    language_en_name = models.CharField(max_length=120)


class AvailableLanguages(models.Model):
    language_code = models.CharField(max_length=120)
